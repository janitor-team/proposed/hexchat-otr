Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hexchat-otr
Source: https://github.com/TingPing/hexchat-otr

Files: *
Copyright: 2014 <adhux@lelantos.org>
	   2014-2016 Patrick Griffis <tingping@tingping.se>
License: GPL-2+

Files: m4/ax_check_compile_flag.m4
       m4/ax_append_flag.m4
       m4/ax_append_compile_flags.m4
Copyright:
 2008, Guido U. Draheim <guidod@gmx.de>
 2011, Maarten Bosmans <mkbosmans@gmail.com>
License: GPL-3 with autoconf exception

Files: src/otr.h
       src/otr_key.c
       src/otr_ops.c
       src/otr_util.c
Copyright: 2008 Uli Meis <a.sporto+bee@gmail.com>
License:   GPL-2+

Files: build-aux/*
Copyright: 1996-2015, Free Software Foundation, Inc
License: GPL-2+

Files: build-aux/install-sh
Copyright: 1994 X Consortium
License: Expat

Files: debian/*
Copyright: 2016 Petter Reinholdtsen <pere@hungry.com>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at your
 option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-3 with autoconf exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at your
 option) any later version.
 .
 As a special exception, the respective Autoconf Macro's copyright
 owner gives unlimited permission to copy, distribute and modify the
 configure scripts that are the output of Autoconf when processing the
 Macro. You need not follow the terms of the GNU General Public
 License when using or distributing such scripts, even though portions
 of the text of the Macro appear in them. The GNU General Public
 License (GPL) does govern all other use of the material that
 constitutes the Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the Autoconf Archive. When you make and distribute
 a modified version of the Autoconf Macro, you may extend this special
 exception to the GPL to apply to your modified version as well.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: Expat
 The MIT License
 .
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
